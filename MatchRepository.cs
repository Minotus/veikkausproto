﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using System.IO;

using RestSharp;

namespace VeikkausAdafySilverlight
{
    class MatchRepository
    {        
        private const string URL_IN_STRING = @"http://veikkausliiga.adafy.com/2014/finland/veikkausliiga/matches";

        private RestClient _client;
       
        public delegate void LoadCompletedHandler(List<RootObject> matches);
        public event LoadCompletedHandler LoadCompleted;

        public MatchRepository()
        {
            _client = new RestClient(URL_IN_STRING);
        }

        public void BeginGetAllMatches()
        {
            var req = new RestRequest();
            _client.ExecuteAsync(req, x => HandleResponse(x));            
        }

        private void HandleResponse(IRestResponse response)
        {
            var matches = JsonConvert.DeserializeObject<List<RootObject>>(response.Content);

            OnLoadCompleted(matches);
        }
        
        private void OnLoadCompleted(List<RootObject> matches)
        {
            if (LoadCompleted != null)
                LoadCompleted(matches);
        }

    }
}
