﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace VeikkausAdafySilverlight
{
    public partial class MatchDetailsPage : PhoneApplicationPage
    {
        public MatchDetailsPage()
        {
            InitializeComponent();

            RootObject match =  (RootObject)Application.Current.Resources["match"];

            title.Text = match.HomeTeam.Name + " vs. " + match.AwayTeam.Name;
            matchDate.Text = match.MatchDateFormatted;
            awayTeamGoals.Text = match.AwayGoals.ToString();
            homeTeamGoals.Text = match.HomeGoals.ToString();

            matchEvents.DataContext = match.MatchEvents;
        }
    }
}