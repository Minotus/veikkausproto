﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using VeikkausAdafySilverlight.Resources;
using System.Collections.ObjectModel;

namespace VeikkausAdafySilverlight
{
    public partial class MainPage : PhoneApplicationPage
    {
        public ObservableCollection<RootObject> MatchInfoList { get; set; }

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            MatchInfoList = new ObservableCollection<RootObject>();
            mainList.DataContext = MatchInfoList;
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            var repository = new MatchRepository();
            repository.LoadCompleted += repository_LoadCompleted;
            repository.BeginGetAllMatches();
        }

        void repository_LoadCompleted(List<RootObject> matches)
        {
            MatchInfoList.Clear();

            foreach(RootObject match in matches)
            {
                MatchInfoList.Add(match);
            }            
        }

        private string ParseMatchInfoStringFromRootObject(RootObject match)
        {
            return match.MatchDateFormatted + " - " + match.HomeTeam.Name + " vs. " + match.AwayTeam.Name;
        }

        private void mainList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mainList.SelectedItem == null)
                return;

            lock (Application.Current.Resources)
            {
                if (Application.Current.Resources.Contains("match"))
                {
                    // Interesting that key cannot be reset, it has to be deleted first.
                    // Otherwise "Notimplementedexception" is thrown :)
                    Application.Current.Resources.Remove("match");
                }

                Application.Current.Resources.Add("match", mainList.SelectedItem);

                // Navigate to detailed page
                NavigationService.Navigate(new Uri("/MatchDetailsPage.xaml", UriKind.Relative));
            }
        }
    }
}